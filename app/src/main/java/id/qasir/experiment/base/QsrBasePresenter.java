package id.qasir.experiment.base;

/**
 * Base method declaration for presenters
 * This class should be extended by the Presenter Contract declaration
 */

public interface QsrBasePresenter<T extends QsrBaseView> {

  void subscribe();

  void unsubscribe();

  void onResume();

  void onDestroy();

  void setTargetView(T targetView);

}
