package id.qasir.experiment.base;

import android.os.Bundle;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public abstract class QsrBaseActivity<T extends ViewDataBinding> extends AppCompatActivity {

  private int containerView = 0;

  private FragmentManager fragmentManager;

  protected T binding;

  /**
   * Abstract function to specify if the activity uses Databinding or not
   *
   * @return true if using DataBinding, otherwise false
   */
  protected abstract boolean useDatabinding();

  /**
   * Used to specify which xml layout is used in this specific activity
   *
   * @return int id of xml layout
   */
  @LayoutRes
  protected abstract int layout();

  /**
   * As a Pattern, used for instantiate class or define all module. note: after this method
   * implemented into class child, don't forget call this method inside OnCreate, OnCreateView, etc
   *
   * @param bundle the bundle
   */
  protected abstract void init(Bundle bundle);

  /**
   * As a Pattern, define data / load data at first time cycle . note: after this method implemented
   * into class child, don't forget call this method inside OnCreate, OnCreateView, etc
   *
   * @param bundle the bundle
   */
  protected abstract void initData(Bundle bundle);

  /**
   * As a Pattern, define object listener, exp: onClickListener, onChangedListener, etc. note: after
   * this method implemented into class child, don't forget call this method inside OnCreate,
   * OnCreateView, etc
   *
   * @param bundle the bundle
   */
  protected abstract void initObjectListener(Bundle bundle);

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
//    instantiate fabric crashlytics
//    CrashlyticsHelper.setExtraData(this);
//    OrientationHelper.setOrientation(this);

    if (useDatabinding()) {
      binding = DataBindingUtil.setContentView(this, layout());
    } else {
      setContentView(layout());
    }

    super.onCreate(savedInstanceState);

    init(savedInstanceState);
    initData(savedInstanceState);
    initObjectListener(savedInstanceState);

  }
  /**
   * Sets fragment container view.
   *
   * @param containerView the container view
   */
  protected void setFragmentContainerView(int containerView) {
    this.containerView = containerView;
  }

  /**
   * Replace fragment.
   *
   * @param fragment the fragment
   * @param bundle the bundle
   * @param addToBackStage the add to back stage
   */
  public void replaceFragment(Fragment fragment, Bundle bundle, boolean addToBackStage) {
    replaceFragment(fragment, bundle, addToBackStage, null);
  }

  /**
   * Replace fragment.
   *
   * @param fragment the fragment
   * @param addToBackStage the add to back stage
   */
  public void replaceFragment(Fragment fragment, boolean addToBackStage) {
    replaceFragment(fragment, null, addToBackStage, null);
  }

  /**
   * Replace fragment.
   *
   * @param fragment the fragment
   * @param bundle the bundle
   * @param addToBackStack the add to back stack
   * @param fragmentManager the fragment manager
   */
  public void replaceFragment(@NonNull Fragment fragment, @Nullable Bundle bundle,
      @Nullable Boolean addToBackStack, @Nullable FragmentManager fragmentManager) {
    if (containerView != 0) {

      // set bundle if exits
      if (bundle != null) {
        if (fragment.getArguments() != null) {
          fragment.getArguments().putAll(bundle);
        } else {
          fragment.setArguments(bundle);
        }
      }

      if (fragmentManager == null) {
        this.fragmentManager = getSupportFragmentManager();
      } else {
        this.fragmentManager = fragmentManager;
      }

      if (addToBackStack == null) {
        addToBackStack = true;
      }

      FragmentTransaction ft = this.fragmentManager.beginTransaction();
      ft.replace(containerView, fragment, fragment.getTag());

      if (addToBackStack) {
        ft.addToBackStack(fragment.getClass().getName());
      }

      ft.commitAllowingStateLoss();
    }
  }

  /**
   * Add fragment.
   *
   * @param fragmentManager the fragment manager
   * @param fragment the fragment
   * @param bundle the bundle
   * @param addToBackStack the add to back stack
   */
  public void addFragment(@NonNull FragmentManager fragmentManager, @NonNull Fragment fragment,
      @NonNull Bundle bundle,
      @NonNull Boolean addToBackStack) {
    addFragment(fragment, bundle, addToBackStack, fragmentManager);
  }

  /**
   * Add fragment.
   *
   * @param fragment the fragment
   * @param bundle the bundle
   * @param addToBackStack the add to back stack
   */
  public void addFragment(@NonNull Fragment fragment, @NonNull Bundle bundle,
      @NonNull Boolean addToBackStack) {
    addFragment(fragment, bundle, addToBackStack, null);
  }

  /**
   * Add fragment.
   *
   * @param fragment the fragment
   * @param addToBackStack the add to back stack
   * @param fragmentManager the fragment manager
   */
  public void addFragment(@NonNull Fragment fragment,
      @NonNull Boolean addToBackStack, @NonNull FragmentManager fragmentManager) {
    addFragment(fragment, null, addToBackStack, fragmentManager);
  }

  /**
   * Add fragment.
   *
   * @param fragmentManager the fragment manager
   * @param fragment the fragment
   * @param addToBackStack the add to back stack
   */
  public void addFragment(@NonNull FragmentManager fragmentManager, @NonNull Fragment fragment,
      @NonNull Boolean addToBackStack) {
    addFragment(fragment, null, addToBackStack, fragmentManager);
  }

  /**
   * Add fragment.
   *
   * @param fragment the fragment
   * @param addToBackStack the add to back stack
   */
  public void addFragment(@NonNull Fragment fragment, @NonNull Boolean addToBackStack) {
    addFragment(fragment, null, addToBackStack, null);
  }

  /**
   * Add fragment.
   *
   * @param fragment the fragment
   * @param bundle the bundle
   * @param addToBackStack the add to back stack
   * @param fragmentManager the fragment manager
   */
  public void addFragment(@NonNull Fragment fragment, @Nullable Bundle bundle,
      @Nullable Boolean addToBackStack, @Nullable FragmentManager fragmentManager) {
    if (containerView != 0) {

      // set bundle if exits
      if (bundle != null) {
        fragment.setArguments(bundle);
      }

      if (fragmentManager == null) {
        this.fragmentManager = getSupportFragmentManager();
      } else {
        this.fragmentManager = fragmentManager;
      }

      if (addToBackStack == null) {
        addToBackStack = true;
      }

      if (containerView != 0) {
        FragmentTransaction ft = this.fragmentManager.beginTransaction();
        ft.add(containerView, fragment, fragment.getTag());
        if (addToBackStack) {
          ft.addToBackStack(fragment.getClass().getName());
        }
        ft.commit();
      }
    }
  }


  @Override
  public void onBackPressed() {
    if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
      getSupportFragmentManager().popBackStack();
      //super.onBackPressed();
    } else {
      finish();
    }
  }

  /**
   * Clear fragment back stacks.
   */
  public void clearFragmentBackStacks() {
    if (fragmentManager == null) {
      fragmentManager = getSupportFragmentManager();
    }

    fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
  }

  public Fragment getCurrentFragmentByFrameId() {
    if (containerView == 0) {
      throw new NullPointerException("Container View ID not set");
    }

    if (fragmentManager == null) {
      fragmentManager = getSupportFragmentManager();
    }

    return fragmentManager.findFragmentById(containerView);
  }

}