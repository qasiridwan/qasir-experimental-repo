package id.qasir.experiment.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import io.reactivex.disposables.CompositeDisposable;

/**
 * There's 2 Params here as generics for Binding and Parent Activity/Fragment
 * @param <T> Parent Activity/Fragment Type (e.g. MainActivity, MainFragment)
 * @param <S> Binding type for main layout (e.g. ActivityMainBinding)
 */
public abstract class QsrBaseFragment<T, S extends ViewDataBinding> extends Fragment {

  /**
   * Replacement for RxDisposableHelper class, the class re-creates the CompositeDisposable,
   * when in reality, we can just call either :
   *    clear() ->  for clearing the CompositeDisposable and we can re-use the same object,
   *                usually called during onStop(), so when the Fragment restarts, we can reuse the
   *                same object
   *    dispose() ->  for clearing and invalidating the CompositeDisposable, cannot be reused,
   *                  usually called during onDestroy(), since the Fragment is destroyed anyway,
   *                  and we will recreate the disposable when the Fragment is recreated
   */
  private CompositeDisposable disposables = new CompositeDisposable();

  public T getParentActivity() {
    return (T) getActivity();
  }

  public T getMyParentFragment() {
    return (T) getParentFragment();
  }

  protected S binding;

  /**
   * Specifies which xml layout to return
   * @return the id of xml layout (e.g. R.layout.activity_main)
   */
  @LayoutRes
  public abstract int layout();

  /**
   * We now call init functions inside onViewCreated, which is called inside the base class.
   * This would help reduce boilerplate code, especially calling onCreate, onCreateView,
   * and onViewCreated. Instead, we now just call the initialization function
   *
   * @param view as passing data view
   * @param bundle as passing data bundle
   */
  public abstract void init(View view, Bundle bundle);

  public abstract void initData(View view, Bundle bundle);

  public abstract void initObjectListener(View view, Bundle bundle);

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    return inflater.inflate(layout(), container, false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    binding = DataBindingUtil.bind(view);

    Bundle bundle = getArguments();
    init(view, bundle);
    initData(view, bundle);
    initObjectListener(view, bundle);
  }

  @Override
  public void onStop() {
    super.onStop();
    disposables.clear();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    disposables.dispose();
  }
}