package id.qasir.experiment.base;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Mainly used for Managing Rx Subscriptions, in case we want to implement RxJava in the future
 * This class should be extended by the Presenter Implementation
 */

public class QsrBasePresenterImpl {

  /**
   * Composite Disposable to contain all the subscriptions called within a presenter.
   */
  private CompositeDisposable disposable = new CompositeDisposable();

  /**
   * Override if necessary, this function will be called when the Presenter is initialized
   */
  public void subscribe() {
  }

  /**
   * Override if necessary, this function will be called when the Presenter is unsubscribed
   */
  public void unsubscribe() {
    disposable.clear();
  }

  /**
   * Override if necessary, this function will be called when the Presenter is destroyed,
   * usually when an activity/fragment is destroyed
   */
  public void onDestroy() {
    disposable.dispose();
  }

  /**
   * Override if necessary, this function will be called when the Presenter is resumed
   */
  public void onResume() {
  }

  /**
   * Call this method to contain subscriptions created inside the presenter
   * @param subscription
   */
  public void manage(Disposable subscription) {
    disposable.add(subscription);
  }

}
