package id.qasir.experiment.views.screens.main;

import id.qasir.experiment.base.QsrBasePresenterImpl;
import id.qasir.experiment.views.screens.main.MainContract.View;

public class MainActivityPresenter extends QsrBasePresenterImpl implements MainContract.Presenter {

  private MainContract.View view;

  @Override
  public void setTargetView(View targetView) {
    this.view = targetView;
  }

  @Override
  public void subscribe() {
    super.subscribe();
    view.showProgress();
  }

  @Override
  public void onResume() {
    super.onResume();
    view.hideProgress();
  }
}
