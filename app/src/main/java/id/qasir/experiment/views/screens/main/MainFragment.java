package id.qasir.experiment.views.screens.main;

import android.os.Bundle;
import android.view.View;
import id.qasir.experiment.R;
import id.qasir.experiment.base.QsrBaseFragment;
import id.qasir.experiment.databinding.FragmentMainBinding;

public class MainFragment extends QsrBaseFragment<MainActivity, FragmentMainBinding> {

  @Override
  public int layout() {
    return R.layout.fragment_main;
  }

  @Override
  public void init(View view, Bundle bundle) {

  }

  @Override
  public void initData(View view, Bundle bundle) {

  }

  @Override
  public void initObjectListener(View view, Bundle bundle) {

  }
}
