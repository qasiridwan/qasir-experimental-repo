package id.qasir.experiment.views.screens.main

import android.os.Bundle
import id.qasir.experiment.R
import id.qasir.experiment.base.QsrBaseActivity
import id.qasir.experiment.databinding.ActivityMainBinding

class MainActivity : QsrBaseActivity<ActivityMainBinding>() {

    override fun useDatabinding() = true

    override fun layout() = R.layout.activity_main

    override fun init(bundle: Bundle?) {
    }

    override fun initData(bundle: Bundle?) {
    }

    override fun initObjectListener(bundle: Bundle?) {
    }

}
