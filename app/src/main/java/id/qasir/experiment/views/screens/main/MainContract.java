package id.qasir.experiment.views.screens.main;

import id.qasir.experiment.base.QsrBasePresenter;
import id.qasir.experiment.base.QsrBaseView;

public interface MainContract {

    interface View extends QsrBaseView {

      void showProgress();

      void hideProgress();
    }

    interface Presenter extends QsrBasePresenter<View> {

    }

}
