package id.qasir.experiment.views.screens.main;

import android.os.Bundle;
import id.qasir.experiment.R;
import id.qasir.experiment.base.QsrBaseActivity;
import id.qasir.experiment.databinding.ActivityMainBinding;

public class MainActivityJava extends QsrBaseActivity<ActivityMainBinding> {

  @Override
  protected boolean useDatabinding() {
    return true;
  }

  @Override
  protected int layout() {
    return R.layout.activity_main;
  }

  @Override
  protected void init(Bundle bundle) {

  }

  @Override
  protected void initData(Bundle bundle) {

  }

  @Override
  protected void initObjectListener(Bundle bundle) {

  }
}
